import time
from pathlib import Path

import selenium
import typer
from loguru import logger
from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from tqdm import tqdm

PATHS_UPDATED = [
    "/",
    "/ncc/",
    "/grid/",
    "/expo/",
    "/news/",
    "/about/",
    "/legal/",
    "/venue/",
    "/events/",
    "/contact/",
    "/keynotes/",
    "/metadata/",
    "/community/",
    "/lre-si-ltp/",
    "/iwltp-2020/",
    "/consortium/",
    "/objectives/",
    "/open-calls/",
    "/styleguide/",
    "/newsletter/",
    "/deliverables/",
    "/registration/",
    "/ncc/intranet/",
    "/expo-projects/",
    "/multilinguism/",
    "/ncc/ncc-italy/",
    "/ncc/ncc-malta/",
    "/ncc/ncc-spain/",
    "/privacy-policy/",
    "/meta-forum-2020",
    "/ncc/ncc-cyprus/",
    "/ncc/ncc-france/",
    "/ncc/ncc-greece/",
    "/ncc/ncc-latvia/",
    "/ncc/ncc-norway/",
    "/ncc/ncc-poland/",
    "/ncc/ncc-serbia/",
    "/ncc/ncc-sweden/",
    "/pilot-projects/",
    "/iwltp-2020/dates",
    "/meta-forum-2019/",
    "/meta-forum-2021/",
    "/meta-forum-2022/",
    "/ncc/ncc-austria/",
    "/ncc/ncc-belgium/",
    "/ncc/ncc-croatia/",
    "/ncc/ncc-denmark/",
    "/ncc/ncc-estonia/",
    "/ncc/ncc-finland/",
    "/ncc/ncc-germany/",
    "/ncc/ncc-hungary/",
    "/ncc/ncc-iceland/",
    "/ncc/ncc-ireland/",
    "/ncc/ncc-romania/",
    "/ncc/ncc-bulgaria/",
    "/ncc/ncc-portugal/",
    "/ncc/ncc-slovakia/",
    "/ncc/ncc-slovenia/",
    "/elg-tutorial-2020/",
    "/iwltp-2020/contact",
    "/ncc/ncc-lithuania/",
    "/2019/10/06/survey/",
    "/contact-meta-forum/",
    "/ncc/ncc-luxembourg/",
    "/about/publications/",
    "/ncc/ncc-netherlands/",
    "/ncc/ncc-switzerland/",
    "/2019/05/24/hackathon/",
    "/iwltp-2020/committees",
    "/iwltp-2020/submission",
    "/meta-forum-2019/expo/",
    "/iwltp-2020/proceedings",
    "/meta-forum-2019/venue/",
    "/meta-forum-2022/venue/",
    "/open-calls/open-call-1",
    "/elt-newsletter-archive/",
    "/meta-forum-2020/header/",
    "/meta-forum-2021/header/",
    "/meta-forum-2022/header/",
    "/ncc/ncc-czech-republic/",
    "/ncc/ncc-united-kingdom/",
    "/open-calls/open-call-2/",
    "/meta-forum-2019/contact/",
    "/meta-forum-2019/gallery/",
    "/meta-forum-2019/keynotes/",
    "/iwltp-2020/call-for-papers",
    "/meta-forum-2019/programme/",
    "/meta-forum-2021/programme/",
    "/meta-forum-2022/programme/",
    "/meta-forum-2019/registration/",
    "/meta-forum-2020/registration/",
    "/meta-forum-2021/project-expo/",
    "/meta-forum-2021/registration/",
    "/meta-forum-2022/project-expo/",
    "/meta-forum-2022/registration/",
    "/2020/02/13/call-for-evaluators/",
    "/open-calls/call-for-evaluators/",
    "/pilot-projects/comprise-example/",
    "/2022/02/17/doubling-the-database",
    "/2019/03/28/launch-of-elg-website/",
    "/5th-national-elg-workshop-germany/",
    "/2020/04/20/deadline-first-open-call/",
    "/2019/01/23/elg-successfully-kicked-off/",
    "/meta-forum-2020/meta-forum-2020-programme/",
    "/2019/10/16/cef-market-study-report-published/",
    "/meta-forum-2020/meta-forum-2020-project-expo/",
    "/2020/03/03/first-call-for-pilot-projects-published/",
    "/2020/10/16/second-open-call-webinar-for-applicants/",
    "/2021/02/22/results-of-open-call-for-pilot-projects-2/",
    "/1st-regional-elg-workshop-switzerland-austria-germany/",
    "/2020/07/28/second-open-call-will-open-in-october-2020/",
    "/2020/07/01/results-of-the-open-call-for-pilot-projects-1/",
    "/2020/10/01/second-open-call-for-pilot-projects-published/",
    "/2019/08/02/2nd-hackathon-sheffield-29-july-02-august-2019/",
    "/2021/05/19/anonymisation-training-day-for-young-researchers",
    "/2022/04/07/5-simple-steps-to-join-the-european-language-grid/",
    "/2020/01/23/qurator-2020-conference-on-digital-curation-technologies/",
    "/2021/10/04/choose-the-right-tool-to-create-your-elg-service-in-python/",
    "/2020/05/06/enormous-interest-in-the-first-elg-open-call-for-pilot-projects/",
    "/2020/12/07/second-open-call-for-pilot-projects-closed-with-103-submissions/",
    "/2021/12/16/how-to-use-the-elg-video-tutorial-for-the-european-language-grid/",
    "/2022/05/12/meta-forum-2022-presents-newest-release-of-european-language-grid/",
    "/2021/10/01/how-does-the-european-language-grid-strengthen-linguistic-diversity/",
    "/2021/07/16/survey-for-lt-developers-and-users-shape-the-future-of-european-language-technology/",
    "/2021/12/20/lowering-language-barriers-how-coreon-uses-the-elg-to-provide-access-to-multilingual-resources/",
]

PATHS = [
    "/",
    "/news/",
    "/objectives/",
    "/deliverables/",
    "/open-calls/",
    "/open-calls/open-call-1",
    "/open-calls/open-call-2/",
    "/pilot-projects/",
    "/about/publications/",
    "/community/",
    "/events/",
    "/consortium/",
    "/contact/",
    "/privacy-policy/",
    "/about/",
    "/lre-si-ltp/",
    "/grid/",
    "/newsletter/",
    "/legal/",
    "/ncc/",
    "/ncc/ncc-austria/",
    "/ncc/ncc-belgium/",
    "/ncc/ncc-bulgaria/",
    "/ncc/ncc-croatia/",
    "/ncc/ncc-cyprus/",
    "/ncc/ncc-czech-republic/",
    "/ncc/ncc-denmark/",
    "/ncc/ncc-estonia/",
    "/ncc/ncc-finland/",
    "/ncc/ncc-france/",
    "/ncc/ncc-germany/",
    "/ncc/ncc-greece/",
    "/ncc/ncc-hungary/",
    "/ncc/ncc-iceland/",
    "/ncc/ncc-ireland/",
    "/ncc/ncc-italy/",
    "/ncc/ncc-latvia/",
    "/ncc/ncc-lithuania/",
    "/ncc/ncc-luxembourg/",
    "/ncc/ncc-malta/",
    "/ncc/ncc-netherlands/",
    "/ncc/ncc-norway/",
    "/ncc/ncc-poland/",
    "/ncc/ncc-portugal/",
    "/ncc/ncc-romania/",
    "/ncc/ncc-serbia/",
    "/ncc/ncc-slovakia/",
    "/ncc/ncc-slovenia/",
    "/ncc/ncc-spain/",
    "/ncc/ncc-sweden/",
    "/ncc/ncc-switzerland/",
    "/ncc/ncc-united-kingdom/",
    "/iwltp-2020/committees",
    "/iwltp-2020/contact",
    "/iwltp-2020/dates",
    "/iwltp-2020/proceedings",
    "/iwltp-2020/submission",
    "/iwltp-2020/call-for-papers",
    "/iwltp-2020/",
    "/5th-national-elg-workshop-germany/",
    "/1st-regional-elg-workshop-switzerland-austria-germany/",
    "/elg-tutorial-2020/",
    "/meta-forum-2019/contact/",
    "/meta-forum-2019/gallery/",
    "/meta-forum-2019/keynotes/",
    "/meta-forum-2019/programme/",
    "/meta-forum-2019/expo/",
    "/meta-forum-2019/registration/",
    "/meta-forum-2019/",
    "/meta-forum-2019/venue/",
    "/meta-forum-2020",
    "/meta-forum-2020/header/",
    "/meta-forum-2020/meta-forum-2020-programme/",
    "/meta-forum-2020/meta-forum-2020-project-expo/",
    "/meta-forum-2020/registration/",
    "/meta-forum-2021/",
    "/meta-forum-2021/header/",
    "/meta-forum-2021/programme/",
    "/meta-forum-2021/project-expo/",
    "/meta-forum-2021/registration/",
    "/2019/08/02/2nd-hackathon-sheffield-29-july-02-august-2019/",
    "/2021/05/19/anonymisation-training-day-for-young-researchers",
    "/2020/02/13/call-for-evaluators/",
    "/2019/10/16/cef-market-study-report-published/",
    "/2021/10/04/choose-the-right-tool-to-create-your-elg-service-in-python/",
    "/2020/04/20/deadline-first-open-call/",
    "/2019/03/28/launch-of-elg-website/",
    "/2020/05/06/enormous-interest-in-the-first-elg-open-call-for-pilot-projects/",
    "/2020/03/03/first-call-for-pilot-projects-published/",
    "/2019/05/24/hackathon/",
    "/2021/10/01/how-does-the-european-language-grid-strengthen-linguistic-diversity/",
    "/2020/01/23/qurator-2020-conference-on-digital-curation-technologies/",
    "/2021/02/22/results-of-open-call-for-pilot-projects-2/",
    "/2020/07/01/results-of-the-open-call-for-pilot-projects-1/",
    "/2020/10/16/second-open-call-webinar-for-applicants/",
    "/2020/12/07/second-open-call-for-pilot-projects-closed-with-103-submissions/",
    "/2020/10/01/second-open-call-for-pilot-projects-published/",
    "/2020/07/28/second-open-call-will-open-in-october-2020/",
    "/2019/01/23/elg-successfully-kicked-off/",
    "/2021/07/16/survey-for-lt-developers-and-users-shape-the-future-of-european-language-technology/",
    "/2019/10/06/survey/",
]

NOT_FOUND_B64 = "iVBORw0KGgoAAAANSUhEUgAAASMAAACtCAMAAADMM+kDAAAAaVBMVEX39/fn5+jm5udlZ3Hy8vLr6+xkZnBeYGv09PT6+vrs7O1bXWj9/f38/PxZXGdYWmbd3d6vsLTDxMdydH3OztGjpaqSk5mqq7CPkZfS0tV8fYW1trltb3iKi5JQU1/HyMubnKKDhYxJTFqPHO3mAAAM1ElEQVR4nO2diZarKBCG3QBFTUxMOp09ue//kFOFS5YGxA2TOf7nzOmeBMvisyig7KuOM2vWrFmzZs2aNWvWrFmzZs2aNbKCgBPXdT0U/CQ8CKZ2aVAFCoWmx3NSkHkWfAKgBvEvVDk4iHUjBZ5CRj6E3P3D58HJ5aagNSIqB/ubNpWig64Jo5AoAVWYSG9KykvQ17CxQqULzYy4HlBph/dzUHkSe4yIsm9NjALXBBEYcntlDrXdPlZbSdnPJkZGQVSa6hFKgdrB7kbbSd3TBkbEHBHYIp0dVMa5PUaafmkZtQBUqKN/ynRpj5E6kvWMWiPqComrDdpipI5kLSPNYUp1G26aEW2JkSaSdYxapOsne10StybObTHSRLKGkc7xTgbV0trr03Nz6Tqr7lInQqjW/mmvhh1G2jGjZNRppAmLrUebNu/ZYaTvkYpRV0Tte6VLl5YY6fOKilHnMGofSLp0aYmRfgZXMeqOqHW3GmoK/Qk0Sh/JKkYdJzWtTYUaTmWDUcOgUfRHF3xl7UvTotVCUuueHUZNLkgZqYPPc0kQhGEYBERdNPFaVNwa4twGo6ZBI2ekrnjx1q0a1LThscCo0QUpI+VRrwESqpq1GGxNiW98Rk2RrGCkOup9DKkgmXescY0xPqPGrbuUkYKspK1iKJsnpCb/xmfUGEZyRoqey0aQ/CIYz/7Na4zRGTWvlqW9ka98pcGhuAqmSbu5RDU6o0YP5IwUnrc4hWHSbo7z0RkZrJaljORN5bGh2G2ZOajfqllhZFBslTKSom21azHsmcGOZ2RGBpEs7bn8OMVc1arxm0yKCyMzMojkaRkZ+Dc2IyMXzBkpztKdkVFxYVxGZi5IGLVLMW2S16uM7k2Ny8jEgykZmaTLkRmZlckmZGSSLkdmZHaXdUJGRv6NysgskidkZHhXYUxGZpE8ISMz/0ZlZOrCVIxM7yqMyMjYhakYmf5RyoiMDD2YjJFhuhyTkfH9sakYGd8HHo+R8Z9XTcXI1L/xGBlH8lSMzO8Dj8ZIFsnmlWcLjGTOKBzsDUMhqdcfxEgW516val1ryc7myVPANIykYdSuJNNXsnPx6HMYycPIKiOpC+EHxZF04m9Z2uspaSQrpttJGMkO4HYZSSP5gxjJ06VVRvJI/iBGiji3yUh2Ii7/fBJGinRpk5Eikj+HkbSy5VhlpIjkz2GkSJcWGcnXHvjNhzBSxrk9RqpI/hhGsubizy3sMVKG0YcwkoaRuKtrjZFq4nc+hZEyXdpjJDsNV39lnZE6XVpjpI5kR72njdI0jR42IsVfy5ZN3lvLGwd/W4rW0nQZWWWkimTCOc+yzCtd9DJUwShKs/XxuHfQ0QDbcbf6smqJzmeZeKpIyq/H49pL4VdeKHu0LsyKI0KnsLsJ0sKueNoG4di0OsITPzzwChUqLuLQiFSRnPyL43ixiE83DymS7WWxWFz2BIOM3xeMsfi0T51ofYlFw8XyXtAkO2h5ORKX/IP2kROtGCr+CSLHX8Rl68uN142FLscwKuwyekyd9HiJL1kEbiwvN8IZtGCeS1aX5YmT4pxxfNp6sis8OCPVxJ8sfSHKcqTIzwz+h63Rp+BEy6+yKFqzop3PziWjFXxCf7lLln6cRckuLr+/R07uV2JbXjcuPjg6TmnXXxzTFOzGyGgJTQnHb+INIUdGc2BUH5TLBtvgjGTn4CUjGses7A45oP9sR8p+sROlQCLBvvgML2odR4KRTwpGKYnx/07wWXyNMI58YXd5ezASh1+O0VHY9fFEgYQRnPyJERyEn90k13hoRsolLDhHf/ebHfPpCT+jghGEh8sP8Ms1xR9FX9jay/Yg98HIZ3siGCXQcXoLsf9AFNNO7NMzNM7qxmyNB++zqLAbnCmETPKXET3wByM856r0bWxGspHGK0a3gAR38BiTJoQVXOIcGYG/h9QRvfAEoywNXVJlhpLRqmS0LfrqnHw/T6IoShc+/eFVa2wcA06QG8Fxh8RJ92DxWDFKnxjRjBeMrtBik0ZJ6dvIjFRFh5IRdzn0cZG5cOno6Rdcgr5xuHznxIk2NaN99BSOBSN65zWjBU4BB2QEdiPB6LlxvBfAvCjGUHMiiLQHo/DBCAZryWiDjIAf2F6Oz0i9hC0ZkZLRitGD6I+Io4JR7C/UjJhbM1r2YMSfGEHyEYyCitFO+DYyI80S9pUR/6HsB92Gia1mRE9UzojmBxpv+ACMgpoRPVOaB4KRY5WRZqv2xuhA2W4fY5qpGDlRkiRi7mdZEtbpqGC0ZTAbDsCI1IzYDv7LkFGQWmUkIVT9o80qH93QD+KBg0e36F7JKAVE0CNkdNxv1uvKWcHoGtNDYMxIrNRljBzvweh4p2yNjKLIJiPNVq2c1zjPKcwnJItxCQcRf6gY8RuKFOsjWEYvV4+pip5ITJm3MGNEf28/Pz+eK2EEJ3swArs3ZJRYZaTO2MUa8nC7QbKkB0Ku4HfGYa5lFSM3BnhLkY+KJe8LowDG5pWZMYL8zthCysh9ZrSPaW6dkSZjl3sRhivH5Vqsb33Cf4rcVDBiuD0oGMFuanl5ZQQJ6UZNGVEqZQTr0GdGGMY3iGSrjNQ12ooRer/ciayEq1zwKb6Scqz93v3nNeQjEAWjDVx0Q0b0fj6f7zJGm2dGq+AMQWSbkS6MBKMcnN/ucWl9p7g4Fvm5ZOQk+8c6O3pnxF2KIdgvZ78x4rif8e0y0kz8TpmzQ17uGsR1DCBzwzquXh8p19nAiJ/Fyrjf3P/GKBMVBKuMJISe/nFnOfcXynD3CKK4xzBiVKy2B2VESE4tM9JN/G+MyCYWuUlsLA0Z7ZcPRuEwjHCtZpeRbuJ/Z4SZAIs84OPSwz3tPXWiq46RS051PopJ5ES5EaM7MMpqRldRhHrE0Tp+YZTAzDkqI+3E/8YINv/0dF2vrz94cYMTFo4S9JCSImcnkUi7z4xghycYiQJRkngi2+oZgV0KdosjsP6xDfhV7H8KRjjka0Z7sIlhrap/DSHtxP/OCKa1e0AIzizsyBHOYYWBn4uaqn9A5be6DiniCKuKcZbu8dpvdzmuMlMtIwft5qubWHdFZAEEfrYwky33FSNyqBn5+QEro+w+Zh1SQujl+TovYy2noiiKeQn2qmJ+EVP7Oi3r2biQOr8wElU52JWm2BUqyiXCvJpR6FV22Q/QPIsVOKzscftTMtqxmlFZ+d5IUsZQjGQT/4vt5MLiqi8ZY8V2LIMP79w5YinZp4sb3hdZskJxxWi3ZMiInOCoLIrcnAlKbJMKRmDi94nRki1LRkGyLuzGB6DJsxx3O5SdcEdChQN8DyfDvUh1Tn895n0RienXpzpEu9XqWnqQbbdbUa0m8MuOBEl2y/P8FzsdZdAOBF8ci9bkim3g5xp+unh/8njP88POTWu7x0chBRpvi7QLyTDxtoc8P6+dCNIl8VaH0+mww9tDZLfdbogXhHCmY1qdc3UNpH/EPhCjhokflYLqrtT1ZxB0BktHSSLuleKd1TR1CHkUtMtf4UdSWMLG9e3XNE2eevY4rrj9K5o6RZyLu5vkqVnhVHXONI1GvU/bMPFXknlg536/rMHfp5eMyahp4td4aoeRQZyr+jEUI/1W7aHJGBnG+ZiMZIZlz+GZipFpnI/IyDCSp2Mkna4khkZkZBjJkzGShpEszsdjZBrJkzEyTZcjMjKN5MkYyb6WPrZsNEbGkTwVI+N0OR4j6VYtlErB6NFgFEbyf/Iok+K04V8H2zKS2pVKyuipbT1CB2XUcNI2DnZ9VZDho1eMZI9RN3V959TMaGb0rJlRs2ZGzZoZNWtm1KyZUbNmRs2aGTVrZtSsmVGzZkbNmhk165MYDfqM8QH9+wJG3Z5VP6B/n8Ro0PdCDOjfFzBSvF/k8xkRedmzV6221btnmt5po6jL2qzVmsv8uX6qFz1Jzcp71qU7dp99KJc5I8X4kcV6i6ZN+i5GqreqSZoqWpq8OuNd38VI9R6ivyNIkY06dezLGMl77v7JmkQBs/17xZ2vY6R6ArhHnheSoQpRp3T0bYw076clQYEpDJRB1Or9tE0n/VhGupVfuY7Rvui4k3/fxqjHa9e7DrWvY2T+sHuZzW5/4PFtjMxfmiBRxz3D1zHqEUgdw+j7GPUIpK5bz+9j1DmQuobRFzLqOrV1nNScr2TUcbR1L/J8I6NuJcTu/n0low4pqXMyUp7uwxm1eCuRibUmfSejtpB6IfpWRk7omlPy3B4DzfleRpoy2h9LfW9bfC8jJzAKJc/tNc5QX8wIV5NNlLzuK8eHPoFRmzvUb9LH0gAxhPoERkQqw+6FXF55hA95v1T9OIXcwWGMW1IYcPJ6Z9olPBgI0P9KYVBppjNr1qxZs2bNmjVr1qxZs2YNoP8AfPz1Wgj9X5AAAAAASUVORK5CYII="

HTML_TEMPLATE = """\
<!DOCTYPE html>
<html>
    <head>
        <title>Webpages comparison</title>
    </head>
    <body>
        {body}
    </body>
</html>
"""

COMPARAISON_TEMPLATE = """\
        <div>
            <h1 style="width: 100%; margin: 0; padding-top: 50px; padding-bottom: 50px; text-align: center; border-top: 2px solid black; border-bottom: 2px solid black;">{title}</h1>
            <div style="display: flex;">
                <h2 style="width: 50%; text-align: center;"><a href={img1_alt}>{img1_alt}</a></h2>
                <h2 style="width: 50%; text-align: center;"><a href={img2_alt}>{img2_alt}</a></h2>
            </div>
            <div style="display: flex;">
                <img style="height: fit-content; border-right: 1px solid black;" width="50%" src="data:image/png;base64,{img1_base64}" alt="{img1_alt}" />
                <img style="height: fit-content; border-left: 1px solid black;" width="50%" src="data:image/png;base64,{img2_base64}" alt="{img2_alt}" />
            </div>
        </div>
"""


def init_driver(headless: bool = True, window_width: int = 1920, window_height: int = 1080):
    opts = FirefoxOptions()
    opts.headless = headless
    opts.add_argument(f"window-size={window_width},{window_height}")
    driver = webdriver.Firefox(options=opts)
    return driver


def get_screenshot_base64(
    driver: webdriver.firefox.webdriver.WebDriver,
    url: str,
):
    try:
        driver.get(url)
        time.sleep(5)
        original_size = driver.get_window_size()
        required_width = driver.execute_script("return document.body.parentNode.scrollWidth")
        required_height = driver.execute_script("return document.body.parentNode.scrollHeight")
        driver.set_window_size(required_width, required_height)
        b64 = driver.get_screenshot_as_base64()
        driver.set_window_size(original_size["width"], original_size["height"])
        return b64
    except:
        return NOT_FOUND_B64


def get_comparison(
    driver: webdriver.firefox.webdriver.WebDriver,
    path: str,
    domain1: str = "https://www.european-language-grid.eu",
    domain2: str = "https://dev.european-language-grid.eu",
):
    url1 = domain1 + path
    url2 = domain2 + path
    img1_base64 = get_screenshot_base64(driver, url1)
    img2_base64 = get_screenshot_base64(driver, url2)
    return COMPARAISON_TEMPLATE.format(
        title=path,
        img1_base64=img1_base64,
        img2_base64=img2_base64,
        img1_alt=url1,
        img2_alt=url2,
    )


def main(output_path: str = "public/index.html"):
    logger.debug("Init Firefox")
    driver = init_driver()
    logger.debug("Open the website")
    comparisons = []
    for path in tqdm(PATHS_UPDATED):
        comparisons.append(get_comparison(driver, path))
    body = "\n".join(comparisons)
    html = HTML_TEMPLATE.format(body=body)
    output_path = Path(output_path)
    output_path.parent.mkdir(parents=True, exist_ok=True)
    with open(output_path, "w") as f:
        f.write(html)


if __name__ == "__main__":
    typer.run(main)
